# soapxx

### Installation
```bash
git clone https://github.com/capoe/soapxx.git
cd soapxx
./build.sh
source soap/SOAPRC
```

### Dependencies
- boost (python mpi filesystem serialization)
- openmpi
- GSL or MKL (MKL + intel compilers are recommended)
